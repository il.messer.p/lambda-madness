#include <stdlib.h>
#include <stdio.h>


// Helper for shortening cast operation
typedef void* (*fn)(void*);

void* id(void *x){
    return x;
}

// left curried function that takes one argument 'l' and returns a function
// that takes one argument 'r' and returns the 'first' argument 'l' 
// left = \l -> \r -> l
void* left(void *l){
    auto void* f(void *r) { return l; } // should be undefined behaviour
    return f;
}

// left curried function that takes one argument 'l' and returns a function
// that takes one argument 'r' and returns that same argument 'r' \l -> \r -> r
// right = \l -> \r -> r
void* right(void *l){
    //auto void* f(void *r) { return r; } 
    return id;
}

//----------------------------------------
// We can define true and false in terms of left and right
void* (*TRUE)(void*) = left;
void* (*FALSE)(void*) = right;

void* const_TRUE(void* x){ return TRUE; }
void* const_FALSE(void* x){ return FALSE; }

void* not(void* x){
    return ((fn) x(FALSE))(TRUE);
}

void* and(void* x){
    //auto void* f(void* y){ return (void*) (((fn) ((fn)x)(y))(x)); }
    return x == TRUE ? id : const_FALSE; 
}

void* or(void* x){
    //auto void* f(void* y){ return (void*) (((fn) ((fn)x)(x))(y)); }
    return x == FALSE ? id : const_TRUE; 
}


int main(){
    printf("sizeof(fn) == sizeof(void*) : %d\n", sizeof(fn) == sizeof(void*));
    long int r = (long int) ((fn) right((void*)1) )((void*)2);
    long int l = (long int) ((fn) left((void*)1)  )((void*)2);

    printf("right(1)(2):\t%d\n", r);
    printf("left(1)(2):\t%d\n", l);

    puts("----------");

    char* rs = (char*) ((fn) right((void*)"5V") )((void*)"GND");
    char* ls = (char*) ((fn) left((void*)"5V")  )((void*)"GND");

    printf("right(5V)(GND):\t%s\n", rs);
    printf("left(5V)(GND):\t%s\n", ls);

    puts("----------");

    printf("FALSE(\"true\")(\"false\"):\t%s\n", ((fn) FALSE("true"))("false"));

    puts("----------");

    fn f_and_t = (fn) and((void*)FALSE);
    void* (*y)(void*) = (fn) ((fn) and((void*)TRUE))((void*)TRUE);
    void* (*x)(void*) = (fn) ((fn) and((void*)TRUE))((void*)FALSE);
    void* (*p)(void*) = (fn) ((fn) and((void*)FALSE))((void*)TRUE);
    void* (*q)(void*) = (fn) ((fn) and((void*)FALSE))((void*)FALSE);
    fn t_and_f = ((fn) x("true") );
    printf("and(TRUE)(TRUE):\t%s\n",   ((fn) y("true") )("false") );
    printf("and(TRUE)(FALSE):\t%s\n",  ((fn) x("true") )("false") );
    printf("and(FALSE)(TRUE):\t%s\n",  ((fn) p("true") )("false") );
    printf("and(FALSE)(FALSE):\t%s\n", ((fn) q("true") )("false") );

    puts("----------");

    y = (fn) ((fn) or((void*)TRUE))((void*)TRUE);
    x = (fn) ((fn) or((void*)TRUE))((void*)FALSE);
    p = (fn) ((fn) or((void*)FALSE))((void*)TRUE);
    q = (fn) ((fn) or((void*)FALSE))((void*)FALSE);
    printf("or(TRUE)(TRUE): \t%s\n",   ((fn) y("true") )("false") );
    printf("or(TRUE)(FALSE):\t%s\n",  ((fn) x("true") )("false") );
    printf("or(FALSE)(TRUE):\t%s\n",  ((fn) p("true") )("false") );
    printf("or(FALSE)(FALSE):\t%s\n", ((fn) q("true") )("false") );


    return 0;
}
